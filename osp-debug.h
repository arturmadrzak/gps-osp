#ifndef _OSP_DEBUG_H
#define _OSP_DEBUG_H

#ifndef DEBUG

#define CONFIG_DUMP_PROTOCOL 0
/* print function for protocol dump*/
#define logp(...)

#define logd(...)
#define logw(...)
#define loge(...)
#define logi(...)

#else

#include <syslog.h>
#define CONFIG_DUMP_PROTOCOL 1
#define logp(...) syslog(LOG_DEBUG, __VA_ARGS__)

#define logd(...) printf(__VA_ARGS__)
#define logi(...) printf(__VA_ARGS__)
#define logw(...) printf(__VA_ARGS__)
#define loge(...) printf(__VA_ARGS__)

#endif

#endif /* _OSP_DEBUG_H */
