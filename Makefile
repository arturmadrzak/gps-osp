SRCS=osp-transport.c osp.c
OBJS=$(SRCS:.c=.o)
DEPS=$(OBJS:.o=.d)
CFLAGS=-I. -ggdb3 -DDEBUG
NAME=gpsosp

.PHONY: all tools driver lib clean

all: deps tools lib 

lib: lib$(NAME).a

lib$(NAME).a: $(OBJS)
		$(AR) rcs $@ $^

deps: $(OBJS:.o=.d)

tools: driver lib
	$(MAKE) -C tools

driver:
	$(MAKE) -C driver

check: check.o $(OBJS)
		$(CC) $^ -o $@ $(LDFLAGS) -lcheck -lrt -lm

%.d: %.c
		$(CC) -c $(CFLAGS) -MM -MF $@ $<

clean:
		@rm -rf check
		@rm -rf *.o
		@rm -rf *.d
		@rm -rf *.a
		$(MAKE) -C tools clean
		$(MAKE) -C driver clean

ifneq ($(MAKECMDGOALS), clean)
-include $(OBJS:.o=.d)
endif

