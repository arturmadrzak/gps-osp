#include <stdio.h>
#include <stdlib.h>
#include <argp.h>
#include <signal.h>
#include <syslog.h>
#include <string.h>
#include "common.h"

static error_t parse_opt(int key, char *arg, struct argp_state *state);

static char doc[] = "Example of using OSP protocol";
static struct argp_option options[] = {
    {"verbose", 'v', 0, 0, "verbose output"},
    {"device", 't', "DEVICE", 0,  "serial device to be used"},
    {"download", 'd', 0, 0, "poll ephemeries and save them to file"},
    {"upload", 'u', 0, 0, "read ephemeries from file and send"},
    {"file", 'f', "FILE", 0, "use this file to store/load ephemeries"},
    {"split", 's', 0, 0, "split ephemeries into separate files"},
    {"id", 'i', "ID", 0, "satellite id. 0 means all available, 1..32" },
    { 0 }
};
static struct argp argp = { options, parse_opt, 0, doc };

struct arguments {
    char *cmd;
    char *device;
    char *file;
    int verbose;
    int poll;
    int set;
    int id;
    int split;
};

static error_t parse_opt(int key, char *arg, struct argp_state *state)
{
    struct arguments *arguments = state->input;
    switch(key)
    {
        case 'v':
            arguments->verbose = 1;
            break;
        case 't':
            arguments->device = arg;
            break;
        case 'd':
            arguments->poll = 1;
            break;
        case 'u':
            arguments->set = 1;
            break;
        case 'f':
            arguments->file = arg;
            break;
        case 's':
            arguments->split = 1;
            break;
        case 'i':
            arguments->id = strtol(arg, NULL, 10);
            if (arguments->id < 0 && arguments->id > 32)
                argp_error(state, "Invalid satellite id");
            break;
        case ARGP_KEY_ARG:
            if (state->arg_num >= 1)
                argp_usage(state);
            arguments->cmd = arg;
            if (strncmp("eph", arg, 3) && strncmp("alm", arg, 3))
                argp_error(state, "Invalid command");
            break;
        case ARGP_KEY_END:
            if (state->arg_num < 1)
                argp_usage(state);
            break;
        default:
            return ARGP_ERR_UNKNOWN;
    }
    return 0;
}

static int eph_poll(osp_t *osp, char *filename, int svid, int split)
{
    int count = 0;
    ephemeris_t eph[12];
    
    if ((count = osp_ephemeris_poll(osp, svid, eph)) < 0)
        return -1;

    if (split) {
        int i;
        char splitfn[512];
        for(i = 0; i < count; i++) {
            sprintf(splitfn, "%s.%d", filename, eph[i].svid);
            FILE *f = fopen(splitfn, "wb");
            fwrite(&eph[i], sizeof(ephemeris_t), 1, f);
            fclose(f);
            printf("Ephemeries saved to %s\n", splitfn);
        }
    } else {
        FILE *f = fopen(filename, "wb");
        fwrite(eph, sizeof(ephemeris_t), count, f);
        fclose(f);
        printf("%d Ephemeries saved to %s\n", count, filename);
    }
    return 0;
}

static int eph_set(osp_t *osp, char *filename)
{
    ephemeris_t eph[12];
    int count;
    int retval = 0;
    memset(eph, 0, sizeof(eph));
    FILE *f = fopen(filename, "rb");
    count = fread(eph, sizeof(ephemeris_t), 12, f);
    fclose(f);
    while(count--) {
        if (osp_ephemeris_set(osp, &eph[count])) {
            retval = -1;
            break;
        }
    }
    return retval;
}

static int eph(struct arguments *args, components_t *cmps)
{
    if (!args->file)
        args->file = "ephemeris.bin";

    if (args->poll)
        return eph_poll(cmps->osp, args->file, args->id, args->split);

    if (args->set)
        return eph_set(cmps->osp, args->file);
}

static void almanac_poll(osp_t *osp, char *filename)
{
    almanac_t almanac;
    execf(osp_almanac_poll(osp, &almanac));
    FILE *f = fopen(filename, "wb");
    fwrite(&almanac, sizeof(almanac_t), 1, f);
    fclose(f);
}

static void almanac_set(osp_t *osp, char *filename)
{
    almanac_t almanac;
    FILE *f = fopen(filename, "rb");
    fread(&almanac, sizeof(almanac_t), 1, f);
    fclose(f);
    execf(osp_almanac_set(osp, &almanac));
}

static void almanac(struct arguments *args, components_t *cmps)
{
    if (!args->file)
        args->file = "almanac.bin";

    if (args->poll)
        almanac_poll(cmps->osp, args->file);

    if (args->set)
        almanac_set(cmps->osp, args->file);
}

static void sig_ignore(int signum)
{
}

int main(int argc, char* argv[])
{
    int retval = 0;
    components_t cmps;
    struct arguments arguments;
    memset(&arguments, 0, sizeof(arguments));
    arguments.device = DEFAULT_DEVICE;
    arguments.file = NULL;
    arguments.id = 0;

    openlog(NULL, LOG_CONS | LOG_NDELAY, LOG_USER | LOG_LOCAL0);

    signal(SIGUSR1, sig_ignore);

    argp_parse(&argp, argc, argv, 0, NULL, &arguments);

    if (strncmp("eph", arguments.cmd, 3) == 0) {
        init(&cmps, arguments.device);
        retval = eph(&arguments, &cmps);
        deinit(&cmps);
    } else if (strncmp("alm", arguments.cmd, 3) == 0) {
        init(&cmps, arguments.device);
        almanac(&arguments, &cmps);
        deinit(&cmps);
    }

    closelog();
    return retval;
}
