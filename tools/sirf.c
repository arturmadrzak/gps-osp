#include <stdio.h>
#include <stdlib.h>
#include <argp.h>
#include <signal.h>
#include <syslog.h>
#include <string.h>
#include "common.h"

static error_t parse_opt(int key, char *arg, struct argp_state *state);

static char doc[] = "Example of using OSP protocol";
static struct argp_option options[] = {
    {"version", 'v', 0, 0, "print firmware version"},
    {"device", 't', "DEVICE", 0,  "serial device to be used"},
    {"factory", 'f', 0, 0,  "factory reset"},
    {"keep", 'k', 0, 0, "do not clear PROM and XOCW"},
    {"soft", 's', 0, 0, "soft reset"},
    {"init", 'i', 0, 0, "initialize data source"},
    {"eph", 'e', 0, 0, "upload ephemeries (ephemeris.bin)"},
    {"almanac", 'a', 0, 0, "upload almanac (almanac.bin)"},
    {"position", 'p', "LAT,LON,ALT", 0, "init position"},
    {"listen", 'l', 0, 0, "keep listening GPS messages"},
    {"measure", 'm', 0, 0, "exit on GPS fix"},
    {"osp", 'o', 0, 0, "switch to OSP protocol"},
    {"data", 'd', "PATH", 0, "path to read almanac or ephemeries from"},
    { 0 }
};
static struct argp argp = { options, parse_opt, 0, doc };

struct arguments {
    char *device;
    char *data;
    int version;
    int factory;
    int keep;
    int soft;
    int init;
    int eph;
    int alm;
    int32_t lat, lon, alt;
    int listen;
    int measure;
    int osp;
};

static error_t parse_opt(int key, char *arg, struct argp_state *state)
{
    struct arguments *arguments = state->input;
    switch(key)
    {
        case 'v':
            arguments->version = 1;
            break;
        case 't':
            arguments->device = arg;
            break;
        case 'f':
            arguments->factory = 1;
            break;
        case 'k':
            arguments->keep = 1;
            break;
        case 's':
            arguments->soft = 1;
            break;
        case 'i':
            arguments->init = 1;
            break;
        case 'p':
            if (sscanf(arg, "%d,%d,%d",
                &arguments->lat, &arguments->lon, &arguments->alt) != 3) {
                return ARGP_ERR_UNKNOWN;
            }
            break;
        case 'l':
            arguments->listen = 1;
            break;
        case 'm':
            arguments->measure = 1;
            break;
        case 'o':
            arguments->osp = 1;
            break;
        case 'e':
            arguments->eph = 1;
            break;
        case 'a':
            arguments->alm = 1;
            break;
        case 'd':
            arguments->data = arg;
            break;
        case ARGP_KEY_ARG:
        case ARGP_KEY_END:
        default:
            return ARGP_ERR_UNKNOWN;
    }
    return 0;
}

static void sig_ignore(int signum)
{
}

static void switch_osp(io_t *serial, char *dev)
{
    char cmd[] = "$PSRF100,0,115200,8,1,0*04\r\n";
    int err = 0;
    serial_config(serial, dev, B4800);
    if (!err) (err = serial->open(serial));
    if (!err) (err = (serial->write(serial, cmd, sizeof(cmd)) != sizeof(cmd)));
    !err ? printf("> |%s|", cmd) : printf("error: %s\n", strerror(errno));
    usleep(100*1000); /* sending bytes */
    if (!err) (err = (serial->close(serial)));
}

static void eph_set(osp_t *osp, char *filename)
{
    ephemeris_t eph[12];
    int count;
    memset(eph, 0, sizeof(eph));
    FILE *f = fopen(filename, "rb");
    count = fread(eph, sizeof(ephemeris_t), 12, f);
    fclose(f);
    while(count--) {
        osp_ephemeris_set(osp, &eph[count]);
    }
}

static void almanac_set(osp_t *osp, char *filename)
{
    almanac_t almanac;
    FILE *f = fopen(filename, "rb");
    fread(&almanac, sizeof(almanac_t), 1, f);
    fclose(f);
    execf(osp_almanac_set(osp, &almanac));
}

int main(int argc, char* argv[])
{
    int rv;
    char filename[512];
    components_t cmps;
    osp_position_t seed;
    osp_position_t *pseed = NULL;
    struct arguments arguments;
    memset(&arguments, 0, sizeof(arguments));
    memset(&seed, 0, sizeof(seed));
    arguments.device = DEFAULT_DEVICE;
    arguments.data = DEFAULT_DATA_PATH;

    openlog(NULL, LOG_CONS | LOG_NDELAY, LOG_USER | LOG_LOCAL0);

    signal(SIGUSR1, sig_ignore);

    argp_parse(&argp, argc, argv, 0, NULL, &arguments);

    if (arguments.osp) {
        set_osp(arguments.device);
        printf("Switched to OSP protocol\n");
    }

    if (arguments.factory) {
        init(&cmps, arguments.device);
        if (arguments.keep) {
            execf(osp_factory(cmps.osp, true, true));
        } else {
            execf(osp_factory(cmps.osp, false, false));
        }
        deinit(&cmps);
    } else {

        init(&cmps, arguments.device);

        if (arguments.lat || arguments.lon || arguments.alt) {
            seed.lat = arguments.lat;
            seed.lon = arguments.lon,
            seed.alt = arguments.alt,
            pseed = &seed;
        }

        if (arguments.init) {
            execf(osp_init(cmps.osp, arguments.soft, pseed, 0));
            execf(osp_wait_for_ready(cmps.osp));
            sleep(1);
        }

        execf(osp_open_session(cmps.osp, false));

        if (arguments.version) {
            char version[80];
            int rv;
            execf((rv = osp_version(cmps.osp, version)));
            if (!rv) printf("Version: %s\n", version);
        }

        if (arguments.alm) {
            sleep(1);
            sprintf(filename, "%s/almanac.bin", arguments.data);
            almanac_set(cmps.osp, filename);
        }

        if (arguments.eph) {
            sleep(1);
            sprintf(filename, "%s/almanac.bin", arguments.data);
            eph_set(cmps.osp, filename);
        }

        if (arguments.listen) {
            printf("Keep listening. Press any key to exit\n");
            getchar();
        }

        execf(osp_close_session(cmps.osp, false));

        deinit(&cmps);
    }
    closelog();
    return 0;
}
