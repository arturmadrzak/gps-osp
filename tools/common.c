#include <stdlib.h>
#include "common.h"



void set_osp(char *device)
{
    char cmd[] = "$PSRF100,0,115200,8,1,0*04\r\n";
    io_t *serial = serial = serial_alloc();
    int err = 0;
    serial_config(serial, device, B4800);
    if (!err) (err = serial->open(serial));
    if (!err) (err = (serial->write(serial, cmd, sizeof(cmd)) != sizeof(cmd)));
    usleep(100*1000); /* sending bytes */
    if (!err) (err = (serial->close(serial)));
    free(serial);
}

void init(components_t *cmps, char* device)
{
    cmps->serial = serial_alloc();
    cmps->transport = osp_transport_alloc(cmps->serial);
    cmps->driver = driver_alloc(cmps->transport);
    cmps->osp = osp_alloc(cmps->driver, NULL, NULL);
    serial_config(cmps->serial, device, B115200);
    osp_start(cmps->osp);
    usleep(100*1000);
}

void deinit(components_t *cmps)
{
    osp_stop(cmps->osp);
    free(cmps->osp);
    free(cmps->driver);
    free(cmps->transport);
    free(cmps->serial);
}

