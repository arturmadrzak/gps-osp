#ifndef _COMMON_H
#define _COMMON_H

#include "driver/driver.h"
#include "driver/serial-io.h"
#include "osp.h"

#define DEFAULT_DEVICE "/dev/ttyUSB1"
#define DEFAULT_DATA_PATH "."

#define execf(f) \
    if ((f)) {\
        printf(#f ": FAIL\n");\
    } else { \
        printf(#f ": SUCCESS\n");\
    }

typedef struct {
    osp_t *osp;
    io_t *transport;
    io_t *serial;
    driver_t *driver;
} components_t;


void set_osp(char *device);
void init(components_t *cmps, char* device);
void deinit(components_t *cmps);

#endif /* _COMMON_H */
